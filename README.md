# DockerProject (branch: master)

This project includes different services put into different containers to host a website.

## Dependencies

The project relies on the following dependencies:

- Docker: Containerization platform used to package and run the application components.
- Maven: Build automation tool used for building the Java application.
- Git: Version control system used for cloning the project repository.
- MySQL: Database management system used for storing application data.
- Memcached: Distributed memory caching system used for caching data.
- RabbitMQ: Message broker used for asynchronous communication between services.
- Nginx: Web server used for serving static files and reverse proxying to the application.

## Supported Operating Systems

The project can be run on the following operating systems:

- Linux: Any modern Linux distribution with Docker support.
- macOS: macOS with Docker Desktop installed.
- Windows: Windows 10 with Docker Desktop installed.

## Tools to Install

Before running the project, ensure that the following tools are installed:

- Docker: Install Docker from the official website based on your operating system.
- Maven: Install Maven using package manager or download it from the official website.
- Git: Install Git using package manager or download it from the official website.
- MySQL client (optional): Install a MySQL client if you need to interact with the database directly for debugging or administration purposes.

## Docker Files

### appFile

This Dockerfile is responsible for building the application container. It performs the following steps:
1. Uses the `openjdk:11` image as the base image for building the application.
2. Installs Maven and Git to build the application.
3. Clones the `vprofile-project` repository from GitHub and checks out the `docker` branch.
4. Builds the project using Maven.
5. Uses the `tomcat:9-jre11` image as the base image for the application runtime.
6. Removes the default Tomcat webapps.
7. Copies the built WAR file from the BUILD_IMAGE stage to the Tomcat webapps directory.
8. Exposes port 8080.
9. Starts Tomcat using `catalina.sh run` command.

### dbfile

This Dockerfile is responsible for building the database container. It performs the following steps:
1. Uses the `mysql:8.0.33` image as the base image.
2. Sets environment variables for the MySQL root password and database name.
3. Adds a SQL file (`db_backup.sql`) to initialize the database schema.

### webFile

This Dockerfile is responsible for building the web server container. It performs the following steps:
1. Uses the `nginx` image as the base image.
2. Removes the default nginx configuration file.
3. Copies a custom nginx configuration file (`nginvproapp.conf`) to configure reverse proxy to the application container.

## Docker Compose File (docker-compose.yml)

This Docker Compose file orchestrates the deployment of multiple containers for the VProfile project. It defines the following services:

- `vprodb`: MySQL database service.
- `vprocache01`: Memcached service.
- `vpromq01`: RabbitMQ service.
- `vproapp`: Application service running on Tomcat.
- `vproweb`: Web server service running on Nginx.

It specifies the build context and Dockerfile locations for each service, as well as container names, exposed ports, volumes, and environment variables.


The volumes section defines persistent data volumes for the database and application containers.

All port information and container names listed in this file are referenced in the `application.properties` file in the `src/main/resources` directory of the main project.

